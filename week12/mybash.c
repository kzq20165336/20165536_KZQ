#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<string.h>
#include<ctype.h>
#include<fcntl.h>

int main()
{
while(1)
{
printf("20165336mybash:");
fflush(stdout);
    char buffer[1024];
    int read_size = read(1, buffer, sizeof(buffer));
    if (read_size > 0)
    {
        buffer[read_size - 1] = 0;
    }

    char* bash_argv[32] = {NULL};
    int bash_index = 0;
    char* start = buffer;
    while (*start != '\0')
    {
        while (*start != '\0' && isspace(*start))
        {
            *start = '\0';
            start++;
        }
        bash_argv[bash_index++] = start;
        while (*start != '\0' && !isspace(*start))
        {
            start++;
        }
    }
    pid_t pid = vfork();

    if (pid < 0)
    {
        printf("vfork failure\n");
        exit(1);
    }
    else if (pid == 0)
    {
        int i = 0;
        int flag = 0;

        for (; bash_argv[i] != NULL; ++i )
        {
            if (strcmp(">", bash_argv[i]) == 0)
            {
                flag = 1;
                break;
            }
        }

        int copyFd;

        bash_argv[i] = NULL;


        if (flag)
        {
            if (bash_argv[i+1] == NULL)
            {
                printf("command error\n");
                exit(1);
            }

            close(1);

            int fd = open(bash_argv[i+1], O_WRONLY | O_CREAT, 0777);

            copyFd = dup2(1, fd);
        }

        execvp(bash_argv[0], bash_argv);

        if (flag)
        {
            close(1);
            dup2(copyFd, 1);

        }

        exit(1);
    }
    else 
    {
        int status = 0;
        int ret = waitpid(pid, &status, 0);
        if (ret == pid)
        {
            if (WIFEXITED(status))
            {
                printf("exitCode is %d\n", WEXITSTATUS(status));
            }
            else if (WIFSIGNALED(status))
            {
                printf("signal is %d\n", WTERMSIG(status));
            }
        }
    }

}
return 0;
}
